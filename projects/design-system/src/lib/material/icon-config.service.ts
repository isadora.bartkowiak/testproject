import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

import { iconList } from './icon-list';

@Injectable({
  providedIn: 'root',
})
export class IconConfigService {

  private static SVG_PATH = 'assets/icons/';

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
  ) { }

  init(): void {

    iconList.forEach((icon) => {
      this.matIconRegistry.addSvgIcon(
        icon.key,
        this.domSanitizer.bypassSecurityTrustResourceUrl(IconConfigService.SVG_PATH + icon.filename),
      );
    });

  }
}
